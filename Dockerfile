FROM python:3.7-slim
WORKDIR /app
SHELL ["/bin/bash", "-c"]
RUN apt-get update
COPY requirements.txt .

# Create virtual environment "venv"
RUN python3 -m venv /venv

# Activate venv and install dependencies
RUN source /venv/bin/activate && \
    python3.7 -m pip install -r requirements.txt

EXPOSE 5000
